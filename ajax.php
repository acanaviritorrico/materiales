<html>

<head>

<title>Ejemplo sencillo de AJAX</title>

    <link href="css/style.css" rel="stylesheet"/>
	<script src="js/jquery-1.9.1.js"></script>

<script>
    function realizaProceso(info){        
            var detalle = {
                    "codigo" : info
            };
            $.ajax({
                    data:  detalle,
                    url:   'pendientes.php',
                    type:  'post',
                    success:  function (response) {
                            $(document).keypress(function(e) {
                                                                
                                if(e.which == 13) {                                    
                                    $("#resultado").html(response);
                                    
                                }
                            }); 
                    }
            });
    }
</script>

</head>

<body>

Introduce Codigo

<input type="text" id="codigo" autofocus="" onkeyup="realizaProceso($('#codigo').val());return false;" /> 
<input type="button" value="Buscar"/>
<div id="resultado" style="background: red;"></div>
</body>

</html>

